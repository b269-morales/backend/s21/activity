let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
let newUser = prompt("Who would you like to add?");

function addUser(){
    users.push(newUser);
    console.log('Added ' + newUser + ' to the Array:')
    console.log(users);
}
addUser();



/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

let indexCheck = Number(prompt("Which Index Number would you like to check?"));

function checkUserIndex(i){

    if (i <= users.length && i >= 0){
        let usersSlice = users.slice();
    console.log('The user in index number '+ i + ' is ' + usersSlice[i].toUpperCase());
    usersSlice[i] = usersSlice[i].toUpperCase();
    console.log(usersSlice);
    } 
    else {
        alert("Please input an Index Number within the array");
    }
}
checkUserIndex(indexCheck);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

function delLastUser(){
    let lastUserIndex = users[users.length - 1];
    users.pop();
    return lastUserIndex;
}
let lastUserDeleted = delLastUser();
console.log("Removed the last user: "+ lastUserDeleted);
console.log(users);


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function updateUserByIndex (indexNumber, update){
    let userToBeUpdated = users[indexNumber];
    users[indexNumber] = update;
    return userToBeUpdated;
}

let indexNumToUpdate = Number(prompt("What Index Number would you like to replace?"));
let userToUpdate = prompt("What name would you like to replace it with?")
let updateUser = updateUserByIndex(indexNumToUpdate,userToUpdate);
console.log("Replaced " + updateUser + " with " + userToUpdate);
console.log(users);



/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

function deleteAll (consent){
    if (consent == true){
    users.length = 0;
    console.log('Deleted ALL users in the array: ');
    console.log(users); 
    } else {
        console.log('Declined Delete Prompt. Array is kept the same:');
        console.log(users);
    }
}
let confirmation = confirm
deleteAll(confirm('Delete all users in the array?'));


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.
*/

function checkEmpty(){
    if (users.length > 0) {
        return false;
    } else {
        return true;
    }
}

let isUsersEmpty = checkEmpty();
console.log('Is the array empty?');
console.log(isUsersEmpty);

